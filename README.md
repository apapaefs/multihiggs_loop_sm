# Higgs anomalous couplings loop model for MadGraph5_aMC 

## Reference

Andreas Papaefstathiou, Gilberto Tetlalmatzi-Xolocotzi, arXiv:2312.13562, Published in JHEP 06 (2024) 124. 

```
@article{Papaefstathiou:2023uum,
    author = "Papaefstathiou, Andreas and Tetlalmatzi-Xolocotzi, Gilberto",
    title = "{Multi-Higgs boson production with anomalous interactions at current and future proton colliders}",
    eprint = "2312.13562",
    archivePrefix = "arXiv",
    primaryClass = "hep-ph",
    doi = "10.1007/JHEP06(2024)124",
    journal = "JHEP",
    volume = "06",
    pages = "124",
    year = "2024"
}
```


## Prereqs:

You will need MG5_aMC 2.9.15 or 3.5.0. This may not work on other versions (but should work with 2.9.14 and 3.4.2). 

## Getting started: 

1. Patch MadGraph5_aMC@NLO to generate loop x tree-level interference. Put the appropriate patch file, ```loopxtree_X_Y_Z.patch``` one directory above the ```MG5_aMC_vX_Y_Z``` "vanilla" version and in the ```MG5_aMC_vX_Y_Z```  directory execute: 

```
patch -p1 < ../loopxtree_X_Y_Z.patch
```

This should patch your MG5_aMC installation to conform with V. Hirschi's instructions as found here: https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/LoopInducedTimesTree.

Copy the ```heft_loop_sm``` model into the ```MG5_aMC_vX_Y_Z/models``` directory. 

## Example: Higgs boson pair production

MadGraph5_aMC@NLO allows for calculation of the interference terms via one insertion of the operator at the squared matrix-element level:

```
generate g g > h h [noborn=MHEFT QCD] MHEFT^2<=1
```

i.e. schematically: $`|\mathcal{M}|^2 \sim \bullet 1 + \blacksquare c_i`$. 

Alternatively, one can get: the interference terms of one or two insertions with the SM and the squares of one-insertion terms (equivalent to two powers in the EFT operators) via:

```
generate g g > h h [noborn=MHEFT QCD] MHEFT^2<=2
```

where schematically this would correspond to $`|\mathcal{M}|^2 \sim \bullet 1 + \blacksquare c_i + \star c_i c_j`$. 

