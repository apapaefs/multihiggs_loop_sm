from optparse import OptionParser
import matplotlib.ticker as ticker
from matplotlib import container
import random
import scipy
from scipy import stats
from scipy.optimize import fsolve
#from scipy.interpolate import griddata
import sys
from numpy.linalg import inv
from numpy.linalg import eig
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.stats import norm
from scipy.optimize import root
from decimal import *
from matplotlib.ticker import Locator
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import rc
import pickle
import re
from matplotlib.colors import ListedColormap
import string
import subprocess

# function to get template
def getTemplate(basename):
    with open('%s.template' % basename, 'r') as f:
        templateText = f.read()
    return string.Template( templateText )

# write a filename
def writeFile(filename, text):
    with open(filename,'w') as f:
        f.write(text)


##############################################
# GENERATE INPUT                             #
##############################################
        
HerwigInputTemplate = getTemplate('LHC-HH.in')
# loop over the inputs:

cg1 = [0.0]
cg2 = [0.0]
cb1 = [0.0]
cb2 = [0.0]
ct1 = [-1.0, -0.5, -0.4, -0.3, -0.2, -0.1, -0.02, -0.01, 0.0, 0.01, 0.02, 0.1, 0.2, 0.3, 0.4, 0.5, 1.0, 3.0, 5.0]#[0.0]
ct2 = [0.0]#[-1.0, -0.5, -0.4, -0.3, -0.2, -0.1, -0.02, -0.01, 0.0, 0.01, 0.02, 0.1, 0.2, 0.3, 0.4, 0.5, 1.0, 3.0, 5.0]
c6 = [0.0] #[-1.0, -0.5, -0.4, -0.3, -0.2, -0.1, -0.02, -0.01, 0.0, 0.01, 0.02, 0.1, 0.2, 0.3, 0.4, 0.5, 1.0, 3.0, 5.0]
ch = [0.0]

# convert between conventions
cg1_HW = [x/3 for x in cg1]
cg2_HW = [-x/3 for x in cg2]
cb1_HW = cb1
cb2_HW = cb2
ct1_HW = ct1
ct2_HW = ct2
c6_HW = c6
ch_HW = ch

#print('cg2 for HW = ', cg2_HW)

for ccg1 in cg1_HW:
    for ccg2 in cg2_HW:
        for cct1 in ct1_HW:
            for cct2 in ct2_HW:
                for ccb1 in cb1_HW:
                    for ccb2 in cb2_HW:
                        for cc6 in c6_HW:
                            for cch in ch_HW:
                                tag = '-cg1_' + str(ccg1) + '-cg2_' + str(ccg2) + '-ct1_' + str(cct1) + '-ct2_' + str(cct2) + '-c6_' + str(cc6) 
                                parmtextsubs = {
                                    'CG1' : ccg1,
                                    'CG2': ccg2,
                                    'CT1' : cct1,
                                    'CT2': cct2,
                                    'CB1' : ccb1,
                                    'CB2': ccb2,
                                    'C6': cc6,
                                    'CH': cch,
                                    'TAG': tag
                                }
                                hwinputfile = 'LHC-HH' + tag + '.in'
                                print('\t\twriting', hwinputfile)
                                writeFile(hwinputfile, HerwigInputTemplate.substitute(parmtextsubs) )

##############
# RUN HERWIG #
##############
nevents = 100000
xsec_dict = {}
for ccg1 in cg1_HW:
    for ccg2 in cg2_HW:
        for cct1 in ct1_HW:
            for cct2 in ct2_HW:
                for ccb1 in cb1_HW:
                    for ccb2 in cb2_HW:
                        for cc6 in c6_HW:
                            for cch in ch_HW:
                                tag = '-cg1_' + str(ccg1) + '-cg2_' + str(ccg2) + '-ct1_' + str(cct1) + '-ct2_' + str(cct2) + '-c6_' + str(cc6)
                                ctuple = tuple([ccg1, ccg2, cct1, cct2, cc6])
                                hwinputfile = 'LHC-HH' + tag + '.in'
                                hwrunfile = 'LHC-HH' + tag + '.run'
                                outfile = 'LHC-HH' + tag + '.out'
                                print('\t\tHerwig reading:', hwinputfile)
                                readcommand = 'Herwig read ' + hwinputfile
                                print(readcommand)
                                p = subprocess.Popen(readcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
                                for line in iter(p.stdout.readline, b''):
                                    print('\t\t', line, end=' ')
                                out, err = p.communicate()
                                #print out, err
                                print('\t\tHerwig running:', hwrunfile, 'for', nevents, 'events')
                                runcommand = 'Herwig run ' + hwrunfile + ' -N' + str(int(nevents))
                                p = subprocess.Popen(runcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
                                for line in iter(p.stdout.readline, b''):
                                    print('\t\t', line, end=' ')
                                out, err = p.communicate()
                                #print out, err
                                grepcommand = 'grep "MEHiggsPair " ' + outfile
                                print(grepcommand)
                                p = subprocess.Popen(grepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
                                for line in iter(p.stdout.readline,b''):
                                    xsec = str(line.split()[-1])
                                print(ctuple, '\t', xsec)
                                # remove the error and convert to pb
                                xsec = xsec.replace('b','')
                                firstDelPos=xsec.find("(")
                                secondDelPos=xsec.find(")")
                                xsec = xsec.replace(xsec[firstDelPos:secondDelPos+1],"")
                                xsec = float(xsec.replace("b","").replace("'", ""))*1000. # convert to pb
                                xsec_dict[ctuple] = xsec
        

print(xsec_dict)
xsecs_array = []
for key in sorted(xsec_dict.keys()):
    print(key, float(xsec_dict[key]))
    xsecs_array.append(float(xsec_dict[key]))
print(xsecs_array)
